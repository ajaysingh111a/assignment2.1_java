/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Moskov
 */
@WebServlet(urlPatterns = {"/ListServlet"})
public class ListServlet extends HttpServlet {  

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        
      try {
            Class.forName("com.mysql.jdbc.Driver");
           
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ass2","root","");
                Statement s=con.createStatement();
     
      ResultSet rs=s.executeQuery("Select * from ass2");
      
   request.setAttribute("rs", rs);     
      
      }     
      
      catch(Exception e){
          
      }
       request.getRequestDispatcher("List.jsp").forward(request, response);
      
    }
}
