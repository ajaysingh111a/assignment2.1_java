/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Moskov
 */
@WebServlet(urlPatterns = {"/create"})
public class CreateServlet extends HttpServlet {

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            PrintWriter out= response.getWriter();
           
        String id=request.getParameter("id");
        String name=request.getParameter("name");
        String phone=request.getParameter("phone");
        String age=request.getParameter("age");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/ass2","root","");
            String sql="Insert into ass2(id,name,Phone,age)"+"Values('"+id+"','"+name+"','"+phone+"','"+age+"')";
            PreparedStatement ps = con.prepareStatement(sql);
            
            if(ps.executeUpdate()>0){
                out.println("Insert successfull");
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CreateServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CreateServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
}
